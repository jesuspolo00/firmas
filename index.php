<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
<link rel="stylesheet" href="main.css">
<h1>
  Firma Digital
</h1>
<div class="wrapper">
  <canvas id="signature-pad" class="signature-pad form-control" width=400 height=200></canvas>
</div>
<div>
  <button id="save">Guardar</button>
  <button id="clear">Limpiar</button>
</div>

<h1>
  Imagen de Firma
</h1>
<img src="" id="imagen" alt="" style="margin-top: 25%;" class="img-fluid border">
<script src="https://code.jquery.com/jquery-3.6.0.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/signature_pad@4.0.0/dist/signature_pad.umd.min.js"></script>
<script type="text/javascript" src="main.js"></script>
